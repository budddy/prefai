import argparse
import json
import sys
import math
import itertools
from subprocess import run, PIPE, Popen
import io

numNodes = 0
allSets = set()


# Example Execution:
# cat graphFile | python3 ./generate_encoding.py
# Predicates:
# sL(A,B) = A<B for sets
# l(A,B) = A<B for nodes
# sE(A,B) = A~B for sets
# sC(A,B) = B element A, B is a node, A is a set
# v(A) = A is a node
# s(A) = A is a set
# sEx(A) = A is a set with only one element ( for Extension rule)
# nodes are called v_*, sets are called s_*


# generate all possible sets from the graph
def getAllSets(graph, processed, edges, node, current):
    current.add(node)
    if node in edges:
        edges.remove(node)
    allSets = set()
    allSets.add(frozenset(current))
    for a in graph[node]:
        if a not in processed and a not in current:
            edges.add(a)
    for a in edges:
        allSets |= getAllSets(graph, processed, set(edges), a, set(current))
    return allSets


# add atoms for nodes, sets and which vertex is contained in a set
def atoms(allSets):
    ret = ''
    retE = ''
    # add vertices
    for a in allNodes:
        ret += 'v(v_' + str(a) + ').\n'
    ret += '\n'
    # add sets
    for a in range(0, len(allSets)):
        ret += 's(s_' + str(a) + ').\n'
        setIds[allSets[a]] = a
    ret += '\n'
    # set contains vertex
    for a in range(0, len(allSets)):
        for b in allSets[a]:
            ret += 'sC(s_' + str(a) + ',v_' + str(b) + ').\n'
        if len(allSets[a]) == 1:
            retE += 'sEx(s_' + str(a) + ").\n"
    ret += '\n' + retE
    return ret + '\n'


def order():
    ret = "% Set order\n"
    ret += "% set relation is transitive\n"
    # A<B & B<C -> A<C
    ret += "sL(A,C):- sL(A,B); sL(B,C); s(A); s(B); s(C); A!=B; B!=C; A!=C.\n"
    # A=B & B<C -> A<C
    ret += "sL(A,C):- sE(A,B); sL(B,C); s(A); s(B); s(C); A!=B; B!=C; A!=C.\n"

    ret += "% set relation anti-symmetric\n"
    # !(A<B & B<A)
    ret += ":- sL(A,B); sL(B,A); s(A); s(B); A!=B.\n"
    # A=B -> B=A
    ret += "sE(A,B) :- sE(B,A); s(A); s(B); A!=B.\n"
    # !(A<B & B<A)
    ret += ":- sE(A,B); sL(B,A); s(A); s(B); A!=B.\n"

    ret += "% set relation is irreflexive\n"
    # !(A<A)
    ret += ":- sL(A,A); s(A).\n"

    ret += "% set relation is total\n"
    # A<B | B<A | A=B
    ret += "sL(A,B); sL(B,A); sE(A,B); sE(B,A):- s(A); s(B); A!=B.\n"

    return ret + '\n'


# extension rule
def extensionRule():
    ret = '% Extension rule\nsL(A,B):- sEx(A); sEx(B); A!=B; l(C,D); v(C); v(D); sC(A,C); sC(B,D).\n'
    return ret + '\n'


# dominance rule
def dominanceRule():
    ret = '% Dominance Rule\n'
    for x in allNodes:
        for a in allSets:
            AuX = frozenset(a | {x})
            if x not in a and AuX in allSets:
                greater = ''
                for y in a:
                    greater += 'v(v_' + str(y) + ');l(v_' + str(x) + ',v_' + str(y) + ');'
                greater += 'v(v_' + str(x) + ').'
                ret += 'sL(s_' + str(setIds[AuX]) + ',s_' + str(setIds[a]) + '):-' + greater + '\n'

                greater = ''
                for y in a:
                    greater += 'v(v_' + str(y) + ');l(v_' + str(y) + ',v_' + str(x) + ');'
                greater += 'v(v_' + str(x) + ').'
                ret += 'sL(s_' + str(setIds[a]) + ',s_' + str(setIds[AuX]) + '):-' + greater + '\n'
    return ret + '\n'


# independence rule
def independenceRule():
    ret = '% Independence Rule\n'
    for a in allSets:
        for b in allSets:
            if a != b:
                remainingNodes = allNodes - a - b
                for x in remainingNodes:
                    c = frozenset(a | {x})
                    d = frozenset(b | {x})
                    if c in allSets and d in allSets:
                        ret += ':-sL(s_' + str(setIds[a]) + ',s_' + str(setIds[b]) + ');sL(s_' + str(setIds[d]) + ',s_' + str(setIds[c]) + ').\n'
    return ret + '\n'


# strict independence rule
def strictIndependenceRule():
    ret = '% Strict Independence Rule\n'
    for a in allSets:
        for b in allSets:
            if a != b:
                remainingNodes = allNodes - a - b
                for x in remainingNodes:
                    c = frozenset(a | {x})
                    d = frozenset(b | {x})
                    if c in allSets and d in allSets:
                        ret += 'sL(s_' + str(setIds[d]) + ',s_' + str(setIds[c]) + '):-sL(s_' + str(setIds[b]) + ',s_' + str(setIds[a]) + ').\n'
    return ret + '\n'


# parse the input graph
def parseGraph(graphString):
    global numNodes
    graph = {}
    for line in graphString.splitlines():
        if len(line) > 0:
            if line[0] == 'p':
                for i in range(1, int(line.split()[2]) + 1):
                    graph[i] = set()
                numNodes = int(line.split()[2])
            else:
                graph[int(line.split()[0])].add(int(line.split()[1]))
                graph[int(line.split()[1])].add(int(line.split()[0]))
    return graph


parser = argparse.ArgumentParser(description='The script generates an encoding which checks if a graph is orderable.\n' +
                                             'The input graph is expected to be in gr format used by the pace challenge 2017.\n' +
                                             '(https://pacechallenge.wordpress.com/pace-2017/track-a-treewidth/)')
parser.add_argument("--graph", help="path to the graph, the graph will be read from stdin if no path is given")
parser.add_argument("--encoding", help="path to save the encoding")
parser.add_argument("--clasp", help="path to clasp")
parser.add_argument("--gringo", help="path to gringo")
parser.add_argument("-e", "--extension", action="store_true", help="deactivate extension rule")
parser.add_argument("-d", "--dominance", action="store_true", help="deactivate dominance rule")
parser.add_argument("-s", "--sIndependence", action="store_true", help="use strict independence")
parser.add_argument("-i", "--independence", action="store_true", help="deactivate independence")
parser.add_argument("-w", "--wOrderable", action="store_true", help="generate satisfying orders")
parser.add_argument("-t", "--threads", type=int, help="number of threads for clasp")
args = parser.parse_args(sys.argv[1:])

setIds = {}
# read graph from file or via stdin
if args.graph:
    graph = parseGraph(open(args.graph, "r").read())
else:
    graph = parseGraph(sys.stdin.read())
allNodes = set(range(1, numNodes + 1))

encoding = ""
processed = set()
for a in graph:
    allSets = allSets | getAllSets(graph, processed, set(), a, set())
    processed.add(a)
encoding += (atoms(list(allSets)))
encoding += (order())

# check if extension rule is activated
if not args.extension:
    encoding += (extensionRule())

# check if dominance rule is activated
if not args.dominance:
    encoding += (dominanceRule())

# check if independence rule is activated
if not args.sIndependence and not args.independence:
    encoding += (independenceRule())

# check if strict independence rule is activated
if args.sIndependence and not args.independence:
    encoding += (strictIndependenceRule())

encoding += ("#show l/2.\n#show sL/2.\n#show sE/2.\n")

# clasp executable
claspPath = "clasp"
if args.clasp:
    claspPath = args.clasp

# gringo executable
gringoPath = "gringo"
if args.gringo:
    gringoPath = args.gringo

# number of threads used by clasp
threads = 1
if args.threads:
    threads = args.threads

weaklyOrderable = False

#############################################
#      check if graph is not orderable      #
#############################################
nodeOrder = "% Node order\n"
nodeOrder += "% anti-symmetric\n"
nodeOrder += ":- l(A,B); l(B,A); v(A); v(B); A!=B.\n"
nodeOrder += "% item relation is ireflexive\n"
nodeOrder += ":- l(A,A); v(A).\n"
nodeOrder += "% item relation is transitive\n"
nodeOrder += "l(A,C):- l(A,B); l(B,C); v(A); v(B); v(C).\n"
nodeOrder += "% item relation is total\n"
nodeOrder += "l(A,B); l(B,A):- v(A); v(B); A!=B.\n\n"

# add node ordering to encoding
encoding_ = nodeOrder + encoding
# ground the encoding
gringo = run([gringoPath], input=encoding_, encoding='ascii', stdout=PIPE)
ground = ''
for line in gringo.stdout:
    ground += line
clasp = Popen([claspPath, "--outf=2", "-t", str(threads)], stdin=PIPE, stdout=PIPE)
clasp.stdin.write(bytes(ground, encoding='ascii'))
clasp.stdin.close()
output = clasp.stdout.read()
outjson = json.loads(output)
if outjson['Models']['Number'] >= 1:
    weaklyOrderable = True
# no suitable ordering found
if not weaklyOrderable:
    print("not orderable")
    exit(0)
else:
    print("weakly orderable")

#############################################
# iterate over all possible node orderings  #
#############################################
uniquesol = set()
n = 0
for i in list(itertools.permutations(range(1, numNodes + 1))):
    # print progress
    n += 1
    print("\rCheck: " + str(n) + "/" + str(math.factorial(numNodes)), end="")
    nodeOrder = ""
    for a in i:
        for b in i:
            if a != b:
                if (i.index(a) < i.index(b)):
                    orderString = "l(v_" + str(a) + ",v_" + str(b) + ").\n"
                elif (i.index(b) < i.index(a)):
                    orderString = "l(v_" + str(b) + ",v_" + str(a) + ").\n"
                if not orderString in nodeOrder:
                    nodeOrder += "l(v_" + str(a) + ",v_" + str(b) + ").\n"
    # add node ordering to encoding
    encoding_ = nodeOrder + encoding
    # ground the encoding
    gringo = run([gringoPath], input=encoding_, encoding='ascii', stdout=PIPE)
    ground = ''
    for line in gringo.stdout:
        ground += line
    # search for a solution
    clasp = Popen([claspPath, "--outf=2", "-t", str(threads)], stdin=PIPE, stdout=PIPE)
    clasp.stdin.write(bytes(ground, encoding='ascii'))
    clasp.stdin.close()
    output = clasp.stdout.read()
    outjson = json.loads(output)
    if outjson['Models']['Number'] >= 1:
        uniquesol.add(tuple(i))
    else:
        print("\nnot strongly orderable")
        ordering = ""
        for a in i:
            ordering += str(a) + " < "
        print("counter example: " + ordering[:-2])
        exit(0)

if len(uniquesol) == math.factorial(numNodes):
    print("\nstrongly orderable graph")
    exit(0)
